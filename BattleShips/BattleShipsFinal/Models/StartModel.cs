﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleShipsFinal
{
    class StartModel
    {
        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
