﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleShipsFinal
{
    class ShipModels
    {
        public string Name { get; set; }
        public int Length { get; set; }
        public int Counter { get; set; }
    }
}
